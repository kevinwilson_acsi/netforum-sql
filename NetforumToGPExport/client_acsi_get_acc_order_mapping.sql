USE [nfacsitest]
GO
/****** Object:  StoredProcedure [dbo].[client_acsi_get_acc_order_mapping]    Script Date: 6/16/2016 8:48:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
	Change Log:	7/28/2015 - Agilutions - Added left join to org table and modified chapter affiliation criteria
				2/10/16 sspruce_agilut: Filtered out Invoice Details that were paid in full and then returned within the same date
*/

ALTER procedure [dbo].[client_acsi_get_acc_order_mapping]
@lastRun Date
as

--declare @lastRun Date = '7/17/2015'

begin

	IF (EXISTS (SELECT *
                 FROM INFORMATION_SCHEMA.TABLES
                 WHERE TABLE_SCHEMA = 'dbo'
                 AND  TABLE_NAME = 'ACSI_ACC_Orders'))
    begin
		drop table ACSI_ACC_Orders
    end

    create table ACSI_ACC_Orders
	(
		[Type] nvarchar(15) null,
		[OrderNo] nvarchar(38) null,
		[CustomerNo] bigint null,
		[Date] Date null,
		[PONumber] nvarchar(150) null,
		[BillToAddress] nvarchar(500) null,
		[ShipToAddress] nvarchar(500) null,
		[ShipToName] nvarchar(500) null,
		[ShipToContact] nvarchar(500) null,
		[ShipToAddress1] nvarchar(500) null,
		[ShipToAddress2] nvarchar(500) null,
		[ShipToAddress3] nvarchar(500) null,
		[ShipToCity] nvarchar(500) null,
		[ShipToState] nvarchar(500) null,
		[ShipToZip] nvarchar(500) null,
		[ShipToCountry] nvarchar(500) null,
		[Subtotal] money null,
		[Frieght] money null,
		[Misc] money null,
		[Tax]money null,
		[Discount]money null,
		[Total] money null,
		[PaymentFlag] nvarchar(2) null,
		[PaymentAmount] money null,
		[PaymentType] nvarchar(500) null,
		[LineNo] varchar(38) null,
		[Item]  nvarchar(500) null,
		[Description] nvarchar(500) null,
		[Shippable] nvarchar(2) null,
		[Inventoried] nvarchar(2) null,
		[UofM] nvarchar(4) null,
		[UnitPrice] money null,
		[Qty] Decimal(10, 0) null,
		[ExtPrice] money null,
		[Company] nvarchar(3) null,
		[Division] nvarchar(3) null,
		[CostCenter] nvarchar(6) null,
		[Natural] nvarchar(3) null,
		[Activity]nvarchar(3) null,
		[Project] nvarchar(5) null
	)

	--added 10/22/2014 for invoices with only one tax, shipping, or discount line item.
	declare @noline table (
		noline_cst_recno bigint null,
		noline_inv_key nvarchar(38) null,
		noline_inv_code_cp nvarchar(50) null,
		noline_ct int null
	)

	insert into @noline
	select a.*
	from (
		select noline_cst_recno = billCst.cst_recno, noline_inv_key = inv_key, noline_inv_Code_cp = inv_Code_cp, noline_ct = count(ivd_key)
		from ac_invoice  (nolock) inv
		join ac_invoice_detail (nolock) on ivd_inv_key = inv_key
		left join co_customer (nolock) billCst on billCst.cst_key = inv_cst_billing_key
		join ac_batch (nolock) on bat_key = inv_bat_key
		left join vw_organization_chapter on voc_org_cst_key = billCst.cst_key
		where bat_close_flag = 1
		and bat_post_flag = 1
		and bat_post_date >= @LastRun and bat_post_date < dateadd(dd, 1, convert(datetime, @LastRun))
		and isnull(voc_chp_code, '') not in ('EE', 'URB')
		and ISNULL(ivd_void_flag, 0) = 0 and ISNULL(ivd_delete_flag, 0) = 0
		group by billCst.cst_recno, inv_key, inv_Code_cp
		having count(ivd_key) = 1
	) a
	join ac_invoice_detail (nolock) on noline_inv_key = ivd_inv_key and ivd_type in ('Tax', 'Shipping', 'Discount')

	insert into ACSI_ACC_Orders
		select
		[Type] = case when noline_inv_key is not null then 'NO LINE' else dbo.client_acsi_acc_GetOrderType(ivd_key) end,
		[OrderNo] = inv_code_cp,
		[CustomerNo] = billCst.cst_recno,
		[Date] = Cast(inv_trx_date as Date),
		[PONumber] = ISNULL(inv_po_number, ''),
		[BillToAddress] = 'BILLTO',
		[ShipToAddress] = 'SHIPTO',
		[ShipToName] = ISNULL(shipcst.cst_org_name_dn, ''),
		[ShipToContact] = ISNull(REPLACE(shipCst.cst_name_cp, ',', ' '), ''),
		[ShipToAddress1] = shipADr.adr_line1,
		[ShipToAddress2] = case
							when shipADR.adr_line2 is not null
							then shipADR.adr_line2
							else ''
							end,
		[ShipToAddress3] = case
							when shipADR.adr_line3 is not null
							then shipADR.adr_line3
							else ''
							end,
		[ShipToCity] = IsNull(shipADr.adr_city, ''),
		[ShipToState] = IsNull(shipADr.adr_state, ''),
		[ShipToZip] = IsNull(shipADr.adr_post_code, ''),
		[ShipToCountry] = ISNull(shipADr.adr_country, ''),
		[Subtotal] = (select ISNULL(sum(ivd_amount_cp), 0) from ac_invoice_detail where ivd_inv_key = inv_key and ivd_void_flag = 0 and ivd_type not in ('Tax', 'Shipping', 'Discount')),
		[Frieght] = (select IsNull(sum(ivd_amount_cp), 0) from ac_invoice_detail where ivd_inv_key = inv_key and ivd_void_flag = 0 and ivd_type = 'Shipping'),
		[Misc] = 0.00,
		[Tax] = (select IsNull(sum(ivd_amount_cp), 0) from ac_invoice_detail where ivd_inv_key = inv_key and ivd_void_flag = 0 and ivd_type = 'Tax'),
		[Discount] = dbo.client_acsi_GetDiscount(inv.inv_code_cp, 1),
		[Total] = (select (sum(ivd_amount_cp) - (dbo.client_acsi_GetDiscount(inv.inv_code_cp, 1))) from ac_invoice_detail where ivd_inv_key = inv_key and ivd_void_flag = 0 and ivd_type not in ('Discount')),
		[PaymentFlag] = case
							when (select SUM(pyd_amount)
										from ac_invoice
										left join ac_invoice_detail on ivd_inv_key = inv_key
										left join ac_payment_detail on pyd_ivd_key = ivd_key
										where inv_code_cp = inv.inv_code_cp
										and pyd_type not in ('use credit', 'return')) is null
							then 'N'
							else 'Y'
						end,
		[PaymentAmount] = (select ISNULL(SUM(pyd_amount), 0)
							from ac_invoice
							left join ac_invoice_detail on ivd_inv_key = inv_key
							left join ac_payment_detail on pyd_ivd_key = ivd_key
							where inv_code_cp = inv.inv_code_cp
							and pyd_type not in ('use credit', 'return')),
		[PaymentType] = dbo.client_acsi_GetPaymentMethod(inv.inv_code_cp, 0),
		[LineNo] = replace(replace(ivd_key, '{', ''), '}', ''),
		[Item] = ISNULL(prd_code, 'MEMBERSHIP'),
		[Description] = ISNULL(prd_name, ''),
		[Shippable] = ISNULL(prc_shippable_flag, ''),
		[Inventoried] = ISNULL(prd_track_inventory_flag, ''),
		[UofM] = 'EACH',
		[UnitPrice] = ivd_price,
		[Qty] = cast(ivd_qty as Decimal(10, 0)),
		[ExtPrice] = ivd_amount_cp,
		[Company] = SUBSTRING(gla_code, 0, 3),
		[Division] = case

					--deprecated 2/11/2015
						--WHEN cht_code = 'International'
						--	THEN '008'
						--WHEN chp_name = 'Headquarters'
						--	THEN '000'
						--WHEN chp_code IS NULL
						--	THEN '000'
						--ELSE '010'

					--added 2/11/2015
						WHEN (cht_code = 'International' or chp_name = 'English Caribbean')
								and isnull(voc_org_otg_code,'') <> 'Global National'
								and (SUBSTRING(gla_code, 22, 2) <> 'PD')
							THEN '008'
						WHEN chp_name in (
							'California Northern','California Southern','Florida',
							'Mid-America','Northeast','Northwest','Ohio River Valley',
							'Rocky Mountain','South Central','Southeast'
						)
							THEN '010'
						ELSE SUBSTRING(gla_code, 4, 3)

					 end,
		[CostCenter] =
									CASE
									  --in the case that there is no region for the transaction, use default cost CostCenter
										--in the case that this is a Professional Development registration, don't apply a region specific CostCenter
										WHEN (chp_chapter_number IS NOT NULL) and (SUBSTRING(gla_code, 22, 2) <> 'PD')
										THEN
											LEFT('000', 4 - LEN(ISNULL(cast(chp_chapter_number as nvarchar), ''))) + ISNULL(cast(chp_chapter_number as nvarchar), '0')
										ELSE
											SUBSTRING(gla_code, 8, 4)
									end,
		[Natural] = SUBSTRING(gla_code, 13, 3),
		[Activity] = SUBSTRING(gla_code, 17, 3),
		[Project] = SUBSTRING(gla_code, 21, 5)
		from ac_invoice (nolock) inv
		left join ac_invoice_detail (nolock) on ivd_inv_key = inv_key
		left join oe_price (nolock) on prc_key = ivd_prc_key
		left join oe_product (nolock) on prd_key = ivd_prc_prd_key
		left join co_customer (nolock) shipCst on shipCst.cst_key = ivd_cst_ship_key
		left join co_customer (nolock) billCst on billCst.cst_key = inv_cst_billing_key
		left join co_customer_x_address (nolock) shipCXA on shipCXA.cxa_key = ivd_cxa_key
		left join co_customer (nolock) soldToCst on soldToCst.cst_key = inv_cst_key
		left join co_address (nolock) shipADR on shipADR.adr_key = shipCXA.cxa_adr_key
		left join ac_batch (nolock) on bat_key = inv_bat_key
		left join vw_organization_chapter on voc_org_cst_key = billCst.cst_key
		left join co_chapter (nolock) on chp_code = voc_chp_code
		left join ac_gl_account (nolock) on gla_key = prc_gla_revenue_key
		LEFT JOIN co_chapter_type (nolock) ON chp_cht_key = cht_key
		left join ac_invoice_ext (nolock) on inv_key_ext = inv_key
		left join @noline on inv_key = noline_inv_key --added 10/22
		left join co_organization (nolock) on billCst.cst_key = org_cst_key --added 7/28/2015
		left join ac_payment_detail (nolock) on ivd_key = pyd_ivd_key -- 2/10/16 sspruce_agilut
		where
		(
		ivd_type not in ('Tax', 'Shipping', 'Discount')
		and bat_close_flag = 1
		and bat_post_flag = 1
		and isnull(voc_chp_code, '') not in ('EE', 'URB')
		-- filter out multiple org->chp relationships
		and (voc_org_cst_key is null or voc_chp_code is not null
			or org_ogt_code not in ('Global National','Higher Education' --added 7/28/2015
			,'International' ,'National','School','Standalone Early Ed','System Office','ACSI'))
		and ISNULL(ivd_void_flag, 0) = 0
		and ISNULL(ivd_delete_flag, 0) = 0
		--and inv_trx_date >= @LastRun and inv_trx_date < dateadd(dd, 1, convert(datetime, @LastRun))
		and bat_post_date >= @LastRun and bat_post_date < dateadd(dd, 1, convert(datetime, @LastRun))
		and isnull(pyd_type, '') <> 'return' -- 2/10/16 sspruce_agilut
		) or
		inv_key in (select noline_inv_key from @noline) --added 10/22
		order by inv_trx_date desc

	select *
	from ACSI_ACC_Orders
	--for xml path('Order'), root('Orders')
end
