
Declare @Action int --Action = 1 is to fix the data remember to run the COMMIT TRAN statement at the bottm of the script when updating data
set @Action = 0

/*
MCF 10/11/2017 Script to FIX invoice Data 
*/

declare @inv_code int
set @inv_code = '117200' --Given by ACSI 

declare @inv_key uniqueidentifier

set @inv_key = ( select top 1 inv_key from ac_invoice with (NOLOCK) where inv_code = @inv_code)




select 

[t_pyd_amount] = pyd_amount, 
[t_ivd_amount_cp] = ivd_amount_cp, 
[t_ida_amount] = ida_amount, 
[t_ivd_type] = ivd_type,
[t_pyd_void_flag] = pyd_void_flag, 
[t_action] = CASE WHEN ida_amount IS NULL AND ( pyd_amount = ivd_amount_cp) then 'DoNothing'
				  WHEN ida_amount IS NULL AND ( pyd_amount != ivd_amount_cp) then 'UpdayPaymentDetail' 
				  WHEN pyd_amount IS NOT NULL AND ida_amount IS NOT NULL then 'UpdayPaymentDetail' 
				  ELSE 'InsertAndUpdatePaymentDetail' END,
[t_pyd_key] = pyd_key, 
[t_pyd_pay_key] = pyd_pay_key, 
[t_ivd_key] = ivd_key,  
[t_inv_key] = inv_key,
[t_pyd_type] = pyd_type, 
[t_pyd_gla_dr_key] = pyd_gla_dr_key, 
[t_pyd_gla_cr_key] = pyd_gla_cr_key

INTO #AllInvoiceItems
from
ac_invoice with (NOLOCK) 
JOIN ac_invoice_detail with (NOLOCK) on inv_key = ivd_inv_key
LEFT JOIN ac_payment_detail with (NOLOCK) on pyd_ivd_key = ivd_key
LEFT JOIN ac_invoice_detail_additional with (NOLOCK) on ida_ivd_key_product = ivd_key
where inv_key =  @inv_key 
and ivd_type = 'product'
order by 1 desc 



--[t_pyd_new_amount] = CASE WHEN t_ida_amount IS NOT NULL THEN (select top 1 MAX(t_pyd_amount) from #AllInvoiceItems with (NOLOCK) WHERE t_ida_amount IS NOT NULL) - t_ida_amount 
SELECT 
[t_pyd_new_amount] = CASE WHEN t_ida_amount IS NOT NULL THEN t_ivd_amount_cp - t_ida_amount 
						  WHEN t_ida_amount IS NULL and t_pyd_amount != t_ivd_amount_cp THEN t_ivd_amount_cp
														ELSE t_pyd_amount END, 
[t_insert_pyd_pay_key] = CASE WHEN t_pyd_amount IS NULL THEN (select top 1 t_pyd_pay_key from #AllInvoiceItems) 
														ELSE NULL END,
#AllInvoiceItems.*
INTO #UpdateTable
FROM #AllInvoiceItems
ORDER BY t_action 




--From here create a credits insert and debits insert temp tables 
--Move this below the payment details update and run inserts and updats based on tables 


IF (@Action = 0)
BEGIN 
	--select * from #AllInvoiceItems
	select * from #UpdateTable 
	ORDER BY t_action, t_pyd_new_amount
END

IF (@Action = 1)
BEGIN 
	

	
begin tran 


     --BEGIN payment detail inserts and updates
	 INSERT INTO ac_payment_detail
	 (
	 pyd_key, 
	 pyd_pay_key,
	 pyd_ivd_key, 
	 pyd_amount,
	 pyd_type, --'Payment'
	 pyd_void_flag, 
	 pyd_add_user, 
	 pyd_add_date,
	 pyd_distributed_liability_flag
	 )
	 SELECT 
	 newid(),
	 t_insert_pyd_pay_key, 
	 t_ivd_key, 
	 t_pyd_new_amount,
	 'Payment',
	 0,
	 'GS_SD3297',
	 getdate(),
	 0
	 FROM  #UpdateTable with (NOLOCK)
	 WHERE t_action = 'InsertAndUpdatePaymentDetail'

	 --updates

	  UPDATE ac_payment_detail
	  set pyd_amount = t_pyd_new_amount, 
	      pyd_change_user = 'GS_SD3297', 
		  pyd_change_date = getdate()
	  FROM #UpdateTable JOIN ac_payment_detail on pyd_key = t_pyd_key
	  WHERE t_action = 'UpdayPaymentDetail'
	  AND t_pyd_void_flag = 0 --MCF 10/12/2017
----End Payment Detail Insert / Updates

---- Ledger Entries 

SELECT 
[x_pyd_key] = t_pyd_key,
[x_pyd_pay_key] = t_pyd_pay_key, 
[x_action] = CASE WHEN led_key IS NULL THEN 'InsertLedger' 
				  WHEN t_action =  'UpdayPaymentDetail' THEN 'UpdateLedgerAmounts'
				  WHEN t_action = 'DoNothing' THEN 'DoNothing' END,
[x_led_new_amount] = t_pyd_new_amount,
[x_led_key] = led_key,
[x_led_gla_code] = led_gla_code, 
[x_led_gla_atc_key] = led_gla_atc_key,
[x_led_gla_atc2_key] = led_gla_atc2_key,
[x_led_amount] = led_amount, 
[x_led_crdr] = led_crdr,
[x_led_bat_key] = led_bat_key,  
[x_led_trx_type] = led_trx_type, 
[x_led_trx_key] = led_trx_key,
[x_led_trx_key_parent] = led_trx_key_parent,
[x_led_parity_flag] = led_parity_flag,
[x_led_amount_cp] = led_amount_cp
INTO #LedgerInsertUpdates
from #updateTable with (NOLOCK)
LEFT JOIN ac_ledger with (NOLOCK) on led_trx_key = t_pyd_key AND led_delete_flag = 0
UNION 
SELECT 
[x_pyd_key] = pyd_key,
[x_pyd_pay_key] = pyd_pay_key,
[x_action] = CASE WHEN led_key IS NULL THEN 'InsertLedger' END,
[x_led_new_amount] = pyd_amount,
[x_led_key] = null,
[x_led_gla_code] = null, 
[x_led_gla_atc_key] =  null,
[x_led_gla_atc2_key] =  null,
[x_led_amount] =  null,
[x_led_crdr] =  null,
[x_led_bat_key] =  null,  
[x_led_trx_type] =  null, 
[x_led_trx_key] =  null,
[x_led_trx_key_parent] =  null,
[x_led_parity_flag] =  null,
[x_led_amount_cp] =  null
FROM ac_payment_detail with (NOLOCK)
JOIN ac_invoice_detail with (NOLOCK) on pyd_ivd_key = ivd_key
LEFT JOIN ac_ledger with (NOLOCK) on led_trx_key = pyd_key 
WHERE 
led_key IS NULL
and ivd_inv_key = @Inv_key


delete #LedgerInsertUpdates WHERE x_pyd_key IS NULL

--Credits 
SELECT 
x_pyd_key, 
x_pyd_pay_key, 
x_action, 
x_led_gla_code = (select top 1 x_led_gla_code from #LedgerInsertUpdates where x_action = 'UpdateLedgerAmounts' AND x_led_crdr = 'CR'),
x_led_gla_atc_key = (select top 1 x_led_gla_atc_key from #LedgerInsertUpdates where x_action = 'UpdateLedgerAmounts' AND x_led_crdr = 'CR'),
x_led_gla_atc2_key = (select top 1 x_led_gla_atc2_key from #LedgerInsertUpdates where x_action = 'UpdateLedgerAmounts' AND x_led_crdr = 'CR'),
x_led_amount = x_led_new_amount, 
x_led_crdr = 'CR',
x_led_bat_key = (select top 1 x_led_bat_key from #LedgerInsertUpdates where x_action = 'UpdateLedgerAmounts' AND x_led_crdr = 'CR'),
x_led_trx_type = 'payment',
x_led_trx_key = x_pyd_key,
x_led_trx_key_parent = x_pyd_pay_key, 
x_led_parity_flag = -1,
x_led_amount_cp = x_led_new_amount
INTO #CreditInserts
FROM #LedgerInsertUpdates 
WHERE x_action = 'InsertLedger'


--Debits
SELECT 
x_pyd_key, 
x_pyd_pay_key, 
x_action, 
x_led_gla_code = (select top 1 x_led_gla_code from #LedgerInsertUpdates where x_action = 'UpdateLedgerAmounts' AND x_led_crdr = 'DR'),
x_led_gla_atc_key = (select top 1 x_led_gla_atc_key from #LedgerInsertUpdates where x_action = 'UpdateLedgerAmounts' AND x_led_crdr = 'DR'),
x_led_gla_atc2_key = (select top 1 x_led_gla_atc2_key from #LedgerInsertUpdates where x_action = 'UpdateLedgerAmounts' AND x_led_crdr = 'DR'),
x_led_amount = x_led_new_amount, 
x_led_crdr = 'DR',
x_led_bat_key = (select top 1 x_led_bat_key from #LedgerInsertUpdates where x_action = 'UpdateLedgerAmounts' AND x_led_crdr = 'DR'),
x_led_trx_type = 'payment',
x_led_trx_key = x_pyd_key,
x_led_trx_key_parent = x_pyd_pay_key, 
x_led_parity_flag = 1,
x_led_amount_cp = x_led_new_amount
INTO #DebitInserts
FROM #LedgerInsertUpdates 
WHERE x_action = 'InsertLedger'


--select * from #CreditInserts --Remove for final
--select * from #DebitInserts--Remove for final




--1 Insert Credits 
--begin tran  --Remove for final
 INSERT INTO ac_ledger
	 (
	 led_key, 
	 led_gla_code,
	 led_gla_atc_key, 
	 led_gla_atc2_key,
	 led_amount,
	 led_crdr, --CR 
	 led_bat_key,
	 led_trx_type,
	 led_trx_key,
     led_trx_key_parent,
	 led_parity_flag, 
	 --led_amount_cp, 
	 led_add_user, 
	 led_add_date
	 )
	 SELECT 
	 newid(),
	 x_led_gla_code, 
	 x_led_gla_atc_key, 
	 x_led_gla_atc2_key, 
	 x_led_amount,
	 'CR',
	 x_led_bat_key,
	 'payment',
	 x_led_trx_key, 
	 x_led_trx_key_parent,
	 x_led_parity_flag, 
	 --x_led_amount_cp,
	 'GS_3297',
	 getdate()
	 FROM #CreditInserts 



--rollback tran

--2 Insert Debits

--begin tran  --Remove for final
 INSERT INTO ac_ledger
	 (
	 led_key, 
	 led_gla_code,
	 led_gla_atc_key, 
	 led_gla_atc2_key,
	 led_amount,
	 led_crdr, --CR 
	 led_bat_key,
	 led_trx_type,
	 led_trx_key,
     led_trx_key_parent,
	 led_parity_flag, 
	 --led_amount_cp, 
	 led_add_user, 
	 led_add_date
	 )
	 SELECT 
	 newid(),
	 x_led_gla_code, 
	 x_led_gla_atc_key, 
	 x_led_gla_atc2_key, 
	 x_led_amount,
	 'DR',
	 x_led_bat_key,
	 'payment',
	 x_led_trx_key, 
	 x_led_trx_key_parent,
	 x_led_parity_flag, 
	 --x_led_amount_cp,
	 'GS_3297',
	 getdate()
	 FROM #DebitInserts 

	 --select top 20 * 
	 --from ac_ledger with (NOLOCK)
	 --order by led_add_date DESC
	 --Rollback tran
--3 Adjust existing items
	-- select * from #LedgerInsertUpdates

--begin tran 
	 UPDATE ac_ledger 
	 set led_amount = x_led_new_amount,
		 led_change_user = 'GS_3297',
		 led_change_date = getdate()
	 FROM #LedgerInsertUpdates 
	 JOIN ac_ledger with (NOLOCK) on led_key = x_led_key 
	 WHERE x_action = 'UpdateLedgerAmounts'

	 --select * from ac_ledger with (NOLOCK)
	 --where led_change_user = 'GS_3297'

	 
	 DROP TABLE #LedgerInsertUpdates
	 DROP TABLE #CreditInserts 
	 DROP TABLE #DebitInserts
	 --rollback tran
	 
END 

DROP TABLE #UpdateTable
DROP TABLE #AllInvoiceItems

--Commit Tran --Remember to run this when updating 
--Rollback Tran
