select
 
[t_pyd_amount] = pyd_amount,
[t_ivd_amount_cp] = ivd_amount_cp,
[t_ida_amount] = ida_amount,
[t_ivd_type] = ivd_type,
[t_pyd_void_flag] = pyd_void_flag,
[t_action] = CASE WHEN ida_amount IS NULL then 'DoNothing'
      WHEN pyd_amount IS NOT NULL AND ida_amount IS NOT NULL then 'UpdayPaymentDetail'
      ELSE 'InsertAndUpdatePaymentDetail' END,
[t_pyd_key] = pyd_key,
[t_pyd_pay_key] = pyd_pay_key,
[t_ivd_key] = ivd_key,
[t_inv_key] = inv_key,
[t_pyd_type] = pyd_type,
[t_pyd_gla_dr_key] = pyd_gla_dr_key,
[t_pyd_gla_cr_key] = pyd_gla_cr_key
,pyd_add_user
,pyd_change_user
from
ac_invoice with (NOLOCK)
JOIN ac_invoice_detail with (NOLOCK) on inv_key = ivd_inv_key
LEFT JOIN ac_payment_detail with (NOLOCK) on pyd_ivd_key = ivd_key
LEFT JOIN ac_invoice_detail_additional with (NOLOCK) on ida_ivd_key_product = ivd_key
where inv_code = '117200'
and ivd_type = 'product'
order by 1 desc