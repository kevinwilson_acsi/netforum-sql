USE [nfacsitest]
GO
/****** Object:  StoredProcedure [dbo].[remove_non_member_from_Demographics]    Script Date: 6/20/2016 12:04:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shuning Wen
-- Create date: 06/20/2014
-- Description:	Clean Up ACSI Demographic table by removing demographic records for non-members
-- test:
/*   BEGIN TRAN
  USE [netFORUMACSIdev]

   declare @schoolYear nvarchar(4),
   @currentUser nvarchar(64)

   set @schoolYear ='2015'
   set @currentUser ='SWen'
   select count(*) from client_acsi_school_demographics Where a01_school_year_ending = @schoolYear and a01_delete_flag = 0

   select Count(a01_key) from client_acsi_school_demographics
    inner join vw_customer_member_flag
    on a01_org_cst_key = vst_cst_key and vst_member_flag = 0
    Where a01_school_year_ending = @schoolYear and a01_delete_flag = 0

   exec remove_non_member_from_Demographics @schoolYear, @currentUser

   select count(*) from client_acsi_school_demographics Where a01_school_year_ending = @schoolYear and a01_delete_flag = 0
   select count(*) from client_acsi_school_demographics Where a01_school_year_ending = @schoolYear and a01_delete_flag = 1  --288   records

    select Count(a01_key) from client_acsi_school_demographics
    inner join vw_customer_member_flag
    on a01_org_cst_key = vst_cst_key and vst_member_flag = 0
    --Where a01_school_year_ending = @schoolYear and a01_delete_flag = 0     -- 0
    Where a01_school_year_ending = '2015' and a01_delete_flag = 0

   ROLLBACK TRAN
*/

-- =============================================
ALTER PROCEDURE [dbo].[remove_non_member_from_Demographics]

	@schoolYear nvarchar(4),
	@currentUser nvarchar(64)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    	update client_acsi_school_demographics
	set a01_delete_flag = 1,
	a01_change_date = GETDATE(),
	a01_change_user = @currentUser
	where a01_key in

	(select a01_key from client_acsi_school_demographics
    inner join vw_customer_member_flag
    on a01_org_cst_key = vst_cst_key and vst_member_flag = 0
    Where a01_school_year_ending = @schoolYear and a01_delete_flag = 0)

End
