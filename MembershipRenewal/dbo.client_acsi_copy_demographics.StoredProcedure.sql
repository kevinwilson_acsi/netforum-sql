USE [nfacsitest]
GO
/****** Object:  StoredProcedure [dbo].[client_acsi_copy_demographics]    Script Date: 6/16/2016 12:32:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sean Wernimont
-- Create date: 10 July 2013
-- Description:	Procedure copied the most recent ACSI demographic record or creates a new empty demographic record for each organization that receives member benefits.
-- Modified by Shuning 6/12/2015
-- test  6/16/2015
/*
BEGIN TRAN
USE [netFORUMACSIdev]
declare @schoolYeartoCopy nvarchar(4)
declare @newSchoolYear nvarchar(4)
declare  @currentUser nvarchar(20)
set @schoolYeartoCopy ='2015'
set @newSchoolYear = @schoolYeartoCopy + 1
set @currentUser ='SWen'
select count(*)
	from client_acsi_school_demographics
	where a01_school_year_ending = @schoolYeartoCopy and a01_delete_flag = 0
	and a01_org_cst_key in  --(select vst_cst_key from vw_customer_member_flag where vst_member_flag = 1)
    (  -- shuning 6/16 to remove the duplicate records in
    select distinct vst_cst_key from vw_customer_member_flag
   inner join client_acsi_school_demographics on a01_org_cst_key = vst_cst_key and vst_delete_flag = 0
   and vst_member_flag = 1 Where a01_school_year_ending = @schoolYeartoCopy
   )
   select count(*) from co_organization_ext org

exec client_acsi_copy_demographics  @schoolYeartoCopy, @currentUser

select count(*) from client_acsi_school_demographics Where a01_school_year_ending = @newSchoolYear and a01_delete_flag = 0   -- 3574 records
select count(*) from co_organization_ext org
ROLLBACK TRAN

*/
-- =============================================

CREATE PROCEDURE [dbo].[client_acsi_copy_demographics]
	-- Add the parameters for the stored procedure here
	@schoolYearToCopy varchar(4),
	@addUser nvarchar(64)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @entityKey uniqueidentifier

	set @entityKey = NEWID()
	-- add by shuning 6/12/2015
	declare @newSchoolYear nvarchar(4)
    set @newSchoolYear = @schoolYearToCopy + 1

	select *
	into #LastYearDemos
	from client_acsi_school_demographics
	where a01_school_year_ending = @schoolYearToCopy and a01_delete_flag = 0
	and a01_org_cst_key in  --(select vst_cst_key from vw_customer_member_flag where vst_member_flag = 1)
    (  -- shuning 6/16 to remove the duplicate records in
    select distinct a01_org_cst_key from vw_customer_member_flag
   inner join client_acsi_school_demographics on a01_org_cst_key = vst_cst_key
   and vst_member_flag = 1 and vst_delete_flag = 0
   Where a01_school_year_ending = @schoolYearToCopy
    )


	Update #LastYearDemos
	set a01_key = NEWID(),
	a01_entity_key = @entityKey,
	--a01_school_year_ending = 2015,
	a01_school_year_ending = @newSchoolYear,   -- Shuning 6/12/2015
	a01_add_date = GETDATE(),
	a01_add_user = @addUser,
	a01_change_date = null,
	a01_change_user = null
	--a01_delete_flag = 0   shuning 6/17/2015
	-- added if not exist to protect add duplicat data  Shuning
	if not exists( select * from client_acsi_school_demographics demo
		where a01_school_year_ending = @newSchoolYear)
	begin

	insert into client_acsi_school_demographics ([a01_key]
		  ,[a01_org_cst_key]
		  ,[a01_parent_org_cst_key]
		  ,[a01_org_name_current]
		  ,[a01_school_year_ending]
		  ,[a01_actual_org_name_if_using_secure_name]
		  ,[a01_church_sponsor_org]
		  ,[a01_denomination]
		  ,[a01_early_ed_name]
		  ,[a01_year_founded]
		  ,[a01_intl_school_type]
		  ,[a01_num_schools_in_system]
		  ,[a01_num_students_vouchers_st_schol]
		  ,[a01_num_students_qual_free_red_lunch]
		  ,[a01_i20_flag]
		  ,[a01_in_dev_flag]
		  ,[a01_online_ed_flag]
		  ,[a01_homeschool_prog_flag]
		  ,[a01_boarding_school_flag]
		  ,[a01_num_students_boarding_prog]
		  ,[a01_special_needs_prog_flag]
		  ,[a01_urban_school]
		  ,[a01_mbr_other_org]
		  ,[a01_efl_prog_flag]
		  ,[a01_intl_homeschool_support_flag]
		  ,[a01_num_students_efl]
		  ,[a01_num_students_intl_expat]
		  ,[a01_num_students_natl]
		  ,[a01_sens_creative_access_cty_flag]
		  ,[a01_intl_no_mail_flag]
		  ,[a01_intl_censor_words_flag]
		  ,[a01_accred_ind_cst_key]
		  ,[a01_hr_recruit_ind_cst_key]
		  ,[a01_curric_dir_ind_cst_key]
		  ,[a01_stud_act_ind_cst_key]
		  ,[a01_dev_dir_ind_cst_key]
		  ,[a01_test_coord_ind_cst_key]
		  ,[a01_fin_dir_ind_cst_key]
		  ,[a01_tech_dir_ind_cst_key]
		  ,[a01_acsi_liaison_ind_cst_key]
		  ,[a01_ed_chair_ind_cst_key]
		  ,[a01_board_coord_ind_cst_key]
		  ,[a01_enroll_ee_fte]
		  ,[a01_enroll_before_after_care]
		  ,[a01_enroll_kdg]
		  ,[a01_enroll_grade_1]
		  ,[a01_enroll_grade_2]
		  ,[a01_enroll_grade_3]
		  ,[a01_enroll_grade_4]
		  ,[a01_enroll_grade_5]
		  ,[a01_enroll_grade_6]
		  ,[a01_enroll_grade_7]
		  ,[a01_enroll_grade_8]
		  ,[a01_enroll_grade_9]
		  ,[a01_enroll_grade_10]
		  ,[a01_enroll_grade_11]
		  ,[a01_enroll_grade_12]
		  ,[a01_enroll_higher_ed]
		  ,[a01_enroll_ed_dept]
		  ,[a01_num_teachers_ee]
		  ,[a01_num_teachers_ee_assist]
		  ,[a01_num_teachers_elem]
		  ,[a01_num_teachers_mid_jh]
		  ,[a01_num_teachers_hs]
		  ,[a01_num_support_staff]
		  ,[a01_num_professors]
		  ,[a01_num_intl_staff]
		  ,[a01_num_natl_staff]
		  ,[a01_num_first_yr_teachers]
		  ,[a01_num_supp_fin_asst_rec]
		  ,[a01_num_total_faculty]
		  ,[a01_num_total_admins]
		  ,[a01_pc_african_am]
		  ,[a01_pc_am_ind_ak_native]
		  ,[a01_pc_asian]
		  ,[a01_pc_carib_isl]
		  ,[a01_pc_caucasian]
		  ,[a01_pc_hisp_lat]
		  ,[a01_pc_native_hi_pac_isl]
		  ,[a01_pc_other]
		  ,[a01_pc_two_plus]
		  ,[a01_accept_code_cond_stmt_faith_affirm_flag]
		  ,[a01_accept_code_cond_stmt_faith_affirm_date]
		  ,[a01_ee_rec_st_funds]
		  ,[a01_ee_rec_st_subsidies]
		  ,[a01_add_user]
		  ,[a01_add_date]
		  ,[a01_change_user]
		  ,[a01_change_date]
		  ,[a01_delete_flag]
		  ,[a01_entity_key]) (select [a01_key]
		  ,[a01_org_cst_key]
		  ,[a01_parent_org_cst_key]
		  ,[a01_org_name_current]
		  ,[a01_school_year_ending]
		  ,[a01_actual_org_name_if_using_secure_name]
		  ,[a01_church_sponsor_org]
		  ,[a01_denomination]
		  ,[a01_early_ed_name]
		  ,[a01_year_founded]
		  ,[a01_intl_school_type]
		  ,[a01_num_schools_in_system]
		  ,[a01_num_students_vouchers_st_schol]
		  ,[a01_num_students_qual_free_red_lunch]
		  ,[a01_i20_flag]
		  ,[a01_in_dev_flag]
		  ,[a01_online_ed_flag]
		  ,[a01_homeschool_prog_flag]
		  ,[a01_boarding_school_flag]
		  ,[a01_num_students_boarding_prog]
		  ,[a01_special_needs_prog_flag]
		  ,[a01_urban_school]
		  ,[a01_mbr_other_org]
		  ,[a01_efl_prog_flag]
		  ,[a01_intl_homeschool_support_flag]
		  ,[a01_num_students_efl]
		  ,[a01_num_students_intl_expat]
		  ,[a01_num_students_natl]
		  ,[a01_sens_creative_access_cty_flag]
		  ,[a01_intl_no_mail_flag]
		  ,[a01_intl_censor_words_flag]
		  ,[a01_accred_ind_cst_key]
		  ,[a01_hr_recruit_ind_cst_key]
		  ,[a01_curric_dir_ind_cst_key]
		  ,[a01_stud_act_ind_cst_key]
		  ,[a01_dev_dir_ind_cst_key]
		  ,[a01_test_coord_ind_cst_key]
		  ,[a01_fin_dir_ind_cst_key]
		  ,[a01_tech_dir_ind_cst_key]
		  ,[a01_acsi_liaison_ind_cst_key]
		  ,[a01_ed_chair_ind_cst_key]
		  ,[a01_board_coord_ind_cst_key]
		  ,[a01_enroll_ee_fte]
		  ,[a01_enroll_before_after_care]
		  ,[a01_enroll_kdg]
		  ,[a01_enroll_grade_1]
		  ,[a01_enroll_grade_2]
		  ,[a01_enroll_grade_3]
		  ,[a01_enroll_grade_4]
		  ,[a01_enroll_grade_5]
		  ,[a01_enroll_grade_6]
		  ,[a01_enroll_grade_7]
		  ,[a01_enroll_grade_8]
		  ,[a01_enroll_grade_9]
		  ,[a01_enroll_grade_10]
		  ,[a01_enroll_grade_11]
		  ,[a01_enroll_grade_12]
		  ,[a01_enroll_higher_ed]
		  ,[a01_enroll_ed_dept]
		  ,[a01_num_teachers_ee]
		  ,[a01_num_teachers_ee_assist]
		  ,[a01_num_teachers_elem]
		  ,[a01_num_teachers_mid_jh]
		  ,[a01_num_teachers_hs]
		  ,[a01_num_support_staff]
		  ,[a01_num_professors]
		  ,[a01_num_intl_staff]
		  ,[a01_num_natl_staff]
		  ,[a01_num_first_yr_teachers]
		  ,[a01_num_supp_fin_asst_rec]
		  ,[a01_num_total_faculty]
		  ,[a01_num_total_admins]
		  ,[a01_pc_african_am]
		  ,[a01_pc_am_ind_ak_native]
		  ,[a01_pc_asian]
		  ,[a01_pc_carib_isl]
		  ,[a01_pc_caucasian]
		  ,[a01_pc_hisp_lat]
		  ,[a01_pc_native_hi_pac_isl]
		  ,[a01_pc_other]
		  ,[a01_pc_two_plus]
		  ,[a01_accept_code_cond_stmt_faith_affirm_flag]
		  ,[a01_accept_code_cond_stmt_faith_affirm_date]
		  ,[a01_ee_rec_st_funds]
		  ,[a01_ee_rec_st_subsidies]
		  ,[a01_add_user]
		  ,[a01_add_date]
		  ,[a01_change_user]
		  ,[a01_change_date]
		  ,[a01_delete_flag]
		  ,[a01_entity_key] from #LastYearDemos)

	update org
	set org_a01_key_ext = ly.a01_key
	from co_organization_ext org
	inner join #LastYearDemos ly on ly.a01_org_cst_key = org.org_cst_key_ext

	insert into co_organization_ext (org_cst_key_ext, org_a01_key_ext)
	select a01_org_cst_key, a01_key from #LastYearDemos where a01_org_cst_key not in (select org_cst_key_ext from co_organization_ext)

	--select @entityKey
end

drop table #LastYearDemos
END
GO
