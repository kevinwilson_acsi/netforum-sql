BEGIN TRANSACTION

UPDATE dbo.mb_membership
SET mbr_pak_prd_renewal_key = CASE
	--WHEN mbr_pak_prd_renewal_key = '83C3CED4-B398-45CA-93F3-09A3CBF493FE' THEN '72062ced-d1fd-4ce2-9720-d48d8bbb317c' --late
	--WHEN mbr_pak_prd_renewal_key = 'A6A9F53C-BA8D-4A1C-AA2F-01D88D78B5BF' THEN '11e3010f-d230-4b89-9a44-802a8c8f5b23'
	--WHEN mbr_pak_prd_renewal_key = '8F23950B-D580-4CC2-8A36-B6464712287C' THEN 'ae65b02c-38bd-4211-b05b-5d1d5d6f8e93'
	--WHEN mbr_pak_prd_renewal_key = 'EBA7ADA5-47CF-4FD3-9CCA-4B08166212F5' THEN 'ef4178a8-da00-44bd-b041-b31c99418807'
	--WHEN mbr_pak_prd_renewal_key = 'FA4BF9CD-4554-4360-90B3-9B560CC272BD' THEN '80b37ac5-5be9-4d04-adab-62b715d7aecb'
	WHEN mbr_pak_prd_renewal_key = '72062ced-d1fd-4ce2-9720-d48d8bbb317c' THEN '83C3CED4-B398-45CA-93F3-09A3CBF493FE' --renewed
	WHEN mbr_pak_prd_renewal_key = '11e3010f-d230-4b89-9a44-802a8c8f5b23' THEN 'A6A9F53C-BA8D-4A1C-AA2F-01D88D78B5BF'
	WHEN mbr_pak_prd_renewal_key = 'ae65b02c-38bd-4211-b05b-5d1d5d6f8e93' THEN '8F23950B-D580-4CC2-8A36-B6464712287C'
	WHEN mbr_pak_prd_renewal_key = 'ef4178a8-da00-44bd-b041-b31c99418807' THEN 'EBA7ADA5-47CF-4FD3-9CCA-4B08166212F5'
	WHEN mbr_pak_prd_renewal_key = '80b37ac5-5be9-4d04-adab-62b715d7aecb' THEN 'FA4BF9CD-4554-4360-90B3-9B560CC272BD'
	END,
	mbr_change_user = 'MbrRenewPkgUpdate_Late',
	mbr_change_date = GETDATE()
WHERE mbr_key IN (

	SELECT mbr_key
	--cst_recno,
	--mbt_code,
	--prd_name,
	--mbr_pak_prd_renewal_key--,
	--renewed = SUM(CASE WHEN YEAR(mbr_expire_date) > 2012 THEN 1 ELSE 0 END),
	--late_renewals = SUM(CASE WHEN mbr_expire_date < GETDATE() THEN 1 ELSE 0 END)

	FROM dbo.mb_membership (NOLOCK)
	JOIN dbo.mb_member_type (NOLOCK) ON mbr_mbt_key = mbt_key
	JOIN dbo.oe_product (NOLOCK) ON mbr_pak_prd_renewal_key = prd_key
	JOIN dbo.co_customer (NOLOCK) ON mbr_Cst_key = cst_key

	WHERE --mbr_expire_date < GETDATE()  --late
	YEAR(mbr_expire_date) > 2013 --renewed
	AND mbr_terminate_date IS NULL
	AND mbr_delete_flag = 0
	--exclude people w/pro forma membership invoices
	AND NOT EXISTS (
		SELECT inv_key
		FROM ac_invoice (NOLOCK)
		JOIN ac_invoice_detail (NOLOCK) ON ivd_inv_key = inv_key AND ivd_delete_flag = 0 AND ivd_void_flag = 0
		JOIN oe_product_type (NOLOCK) ON ivd_prc_prd_ptp_key = ptp_key AND ptp_code = 'Membership Dues'
		WHERE inv_cst_billing_key = mbr_cst_key
		AND inv_proforma = 1
		AND inv_delete_flag = 0
	)
	AND cst_type = 'organization'
	AND mbr_pak_prd_renewal_key IN
	(
	--'83C3CED4-B398-45CA-93F3-09A3CBF493FE', --late
	--'A6A9F53C-BA8D-4A1C-AA2F-01D88D78B5BF',
	--'8F23950B-D580-4CC2-8A36-B6464712287C',
	--'EBA7ADA5-47CF-4FD3-9CCA-4B08166212F5',
	--'FA4BF9CD-4554-4360-90B3-9B560CC272BD'
	'72062ced-d1fd-4ce2-9720-d48d8bbb317c', --renewed
	'11e3010f-d230-4b89-9a44-802a8c8f5b23',
	'ae65b02c-38bd-4211-b05b-5d1d5d6f8e93',
	'ef4178a8-da00-44bd-b041-b31c99418807',
	'80b37ac5-5be9-4d04-adab-62b715d7aecb'
	)


	--GROUP BY mbt_code,
	--prd_name,
	--mbr_pak_prd_renewal_key

	--ORDER BY mbt_code
)

--COMMIT TRANSACTION
--ROLLBACK TRANSACTION
