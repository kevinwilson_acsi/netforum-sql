USE [nfacsitest]
GO
/****** Object:  StoredProcedure [dbo].[client_acsi_member_directory_R]    Script Date: 2/13/2017 3:08:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Procedure:	client_acsi_member_directory_R
-- Author:		Shuning Wen
-- Create date: 10/30/2014
-- Description:	modified Member Directory Search and Details written by Agilutions
-- search should always be member
-- updated: 11/5/2014 remove @members_only requested by Judy and Walt   Shuning
-- updated: filter sensitive countries     shuning  12/19/2014
-- updated: add country indicator
-- -- test in production   1/5/2015
-- exec client_acsi_member_directory_R  --3497 records 7/21/2015Israel
-- exec client_acsi_member_directory_R @adr_country = 'Israel' -- 3102 records 1 second 7/21/2015
-- exec client_acsi_member_directory_R @adr_country = 'China' -- 3 records 1 second 7/21/2015
-- exec client_acsi_member_directory_O @adr_country = 'China' -- 6 records 1 second 7/21/2015
-- exec client_acsi_member_directory_R @adr_country = 'United States', @adr_city = 'Colorado Springs'  -- 11 rows 1 second 7/21/2015

--
-- exec client_acsi_member_directory_O @adr_country = 'United States',@members_only = 1 -- 3127
-- exec client_acsi_member_directory_R @adr_post_code ='80907'  -- 1 record
--  exec client_acsi_member_directory_R @adr_post_code ='80907'  --1 record
--  exec client_acsi_member_directory_R @adr_country = 'United States', @cst_org_name_dn = 'Abundant Life School' -- 2 record
--  exec client_acsi_member_directory_R @adr_country = 'United States', @cst_org_name_dn = 'Buenos Aires International Christian Academy'
--  exec client_acsi_member_directory_R @cst_key = 'AB7A82E1-3FC7-4E11-B9B6-37EC824471BD'
 -- exec client_acsi_member_directory_O @adr_country = 'United States' -- 20422 records 2 seconds
 -- exec client_acsi_member_directory_R @adr_state = 'CO'  --58 rows 1 second
 -- exec client_acsi_member_directory_O @adr_state = 'CO', @members_only = 1 --58 rows 2 second
 -- exec client_acsi_member_directory_R @org_ogt_code = 'International' --192 rows, 2 seconds
 -- exec client_acsi_member_directory_O @org_ogt_code = 'International', @members_only = 1 --192 rows, 2 seconds
 -- exec client_acsi_member_directory_R @grade_level_select = 'Early Education' --2002 rows 1 second
 -- exec client_acsi_member_directory_O @grade_level_select = 'Early Education', @members_only = 1  --0 rows 1 second

 -- exec client_acsi_member_directory_R @enrollment_low_limit = '50',@enrollment_high_limit = '200' --1592 rows 1 second 7/21/2015
 -- exec client_acsi_member_directory_O @enrollment_low_limit = '50',@enrollment_high_limit = '200', @members_only = 1  --0 rows 1 second
 -- exec client_acsi_member_directory_R @accreditation_select = 'ACSI'   --893 records 7/21/2015
 -- exec client_acsi_member_directory_O @accreditation_select = 'ACSI'   --0 records
 -- exec client_acsi_member_directory_R @special_prog_select = 'Special Needs'  --250 rows  7/21/2015
 -- exec client_acsi_member_directory_O @special_prog_select = 'Special Needs',@members_only = 1  --0 row

 -- exec client_acsi_member_directory_R @cst_org_name_dn = 'Nanjing Echo International School'  -- 0 record correct
 -- exec client_acsi_member_directory_R @cst_org_name_dn = 'Zhongshi Korean International School'  -- 0 record correct
 -- exec client_acsi_member_directory_R @cst_org_name_dn = 'Wuhan Yangtze International School'  -- 0 record correct
 -- exec client_acsi_member_directory_R @cst_org_name_dn = 'Evangelical Christian Academy-Elementary'  -- 1 record correct
-- =============================================

ALTER PROCEDURE [dbo].[client_acsi_member_directory_R]
    @cst_key NVARCHAR(40) = NULL,
    --uniqueidentifier, -- = null , --NVARCHAR(36) = NULL,
    @cst_org_name_dn NVARCHAR(150) = NULL,
    @adr_city NVARCHAR(40) = NULL,
    @adr_state NVARCHAR(40) = NULL,
	@adr_post_code NVARCHAR(20) = NULL,
	@adr_country NVARCHAR(40) = NULL,
	@org_ogt_code NVARCHAR(30) = NULL,
	@grade_level_select NVARCHAR(150) = null,
	@enrollment_low_limit VARCHAR(10) = null,
	@enrollment_high_limit VARCHAR(10) = null,
	@accreditation_select NVARCHAR(150) = null,
	@special_prog_select NVARCHAR(150) = null
with recompile --many parameter variations possible
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Education level flags

	DECLARE @early_ed_flag TINYINT = 0
	, @elementary_sch_flag TINYINT = 0
	, @middle_sch_flag TINYINT = 0
	, @high_sch_flag TINYINT = 0
	, @higher_ed_flag TINYINT = 0
	, @star_flag TINYINT = 0
	, @acsi_flag TINYINT = 0
	, @heap_flag TINYINT = 0
	, @other_accred_flag TINYINT = 0
	, @i_20_flag TINYINT = 0
	, @special_needs_flag TINYINT = 0
	, @efl_flag TINYINT = 0
	, @online_ed_flag TINYINT = 0
	, @homeschool_flag TINYINT = 0
	, @boarding_school_flag TINYINT = 0

	-- set flags based on grade level select parm
	IF CHARINDEX('Early Education',@grade_level_select) > 0
		SET @early_ed_flag = 1

	IF CHARINDEX('Elementary',@grade_level_select) > 0
		SET @elementary_sch_flag = 1

	IF CHARINDEX('Middle School',@grade_level_select) > 0
		SET @middle_sch_flag = 1

	IF CHARINDEX('High School',@grade_level_select) > 0
		SET @high_sch_flag = 1

	IF CHARINDEX('Higher Education',@grade_level_select) > 0
		SET @higher_ed_flag = 1

	-- set flags based on accreditation select

	IF CHARINDEX('Star',@accreditation_select) > 0
		SET @star_flag = 1

	IF CHARINDEX('ACSI',@accreditation_select) > 0
		SET @acsi_flag = 1

	IF CHARINDEX('Heap',@accreditation_select) > 0
		SET @heap_flag = 1

	IF CHARINDEX('Other',@accreditation_select) > 0
		SET @other_accred_flag = 1

	-- set flags based on special programs selected

	IF CHARINDEX('I-20',@special_prog_select) > 0
		SET @i_20_flag = 1

	IF CHARINDEX('Special Needs',@special_prog_select) > 0
		SET @special_needs_flag = 1

	IF CHARINDEX('EFL',@special_prog_select) > 0
		SET @efl_flag = 1

	IF CHARINDEX('Online Education',@special_prog_select) > 0
		SET @online_ed_flag = 1

	IF CHARINDEX('Home',@special_prog_select) > 0
		SET @homeschool_flag = 1

	IF CHARINDEX('Boarding',@special_prog_select) > 0
		SET @boarding_school_flag = 1

	-- Make sure upper and lower limits are either null or integers

	if isnumeric(@enrollment_low_limit) = 0 or @enrollment_low_limit = ''
		set @enrollment_low_limit = null
	else set @enrollment_low_limit = convert(int,@enrollment_low_limit)

	if isnumeric(@enrollment_high_limit) = 0 or @enrollment_high_limit = ''
		set @enrollment_high_limit = null
	else set @enrollment_high_limit = convert(int,@enrollment_high_limit)


    SELECT ocst.cst_key,
    Orgcxa.cxa_key,
    org_name,
    orgAdr.adr_city,
    orgAdr.adr_state,
    orgAdr.adr_country,
    country_adr.adr_country as countryIndicator,  --7/21/2105
	Orgcxa.cxa_mailing_label_html,
	ocst.cst_eml_address_dn,
	ocst.cst_url_code_dn,
    orgAdr.adr_latitude,
    orgAdr.adr_longitude

    FROM dbo.co_customer (NOLOCK) ocst
    JOIN dbo.co_organization(NOLOCK) ON cst_key = org_cst_key AND org_delete_flag = 0
    JOIN vw_customer_member_flag  (NOLOCK)  ON org_cst_key= vst_cst_key  --7/21/2015
    JOIN dbo.co_organization_ext (NOLOCK) ON org_cst_key = org_cst_key_ext
    LEFT JOIN dbo.client_acsi_school_demographics (NOLOCK) ON org_a01_key_ext = a01_key
    LEFT JOIN dbo.co_customer_x_address Orgcxa(NOLOCK) ON cst_cxa_key = Orgcxa.cxa_key  --7/21/2015
	LEFT JOIN dbo.co_address orgAdr (NOLOCK) ON Orgcxa.cxa_adr_key = orgAdr.adr_key AND orgAdr.adr_delete_flag = 0 --7/21/2015
	LEFT JOIN dbo.co_organization_dem (NOLOCK) ON cst_key = odm_cst_key AND odm_delete_flag = 0
	LEFT JOIN co_customer_x_address country_cxa (NOLOCK) ON cst_key = country_cxa.cxa_cst_key and country_cxa.cxa_delete_flag = 0
	and country_cxa.cxa_adt_key = 'E84C4173-E764-4172-82D6-3304E46D4395'
	--E84C4173-E764-4172-82D6-3304E46D4395 is the address type key for Country Indicator addresses
    LEFT JOIN co_address country_adr (NOLOCK) ON country_cxa.cxa_adr_key = country_adr.adr_key and country_adr.adr_delete_flag = 0

	WHERE ocst.cst_delete_flag = 0
	AND ISNULL(org_no_mbr_directory_flag_ext,0)=0
	and vst_member_flag = 1   -- 7/21/2015
	AND org_ogt_code <> 'ACSI'
	--AND orgAdr.adr_country not in (select sen_country from client_acsi_sensitive_country)   --7/21/2015
    --and country_adr.adr_country not in (select sen_country from client_acsi_sensitive_country)
            AND ( @cst_key IS NULL OR ocst.cst_key = @cst_key)
            AND ( @cst_org_name_dn IS NULL OR ocst.cst_org_name_dn LIKE '%' + @cst_org_name_dn + '%')
            AND ( @adr_city IS NULL OR orgAdr.adr_city LIKE '%' + @adr_city + '%')    --7/21/2015
            AND ( @adr_state IS NULL OR orgAdr.adr_state = @adr_state)
            AND ( @adr_post_code IS NULL OR orgAdr.adr_post_code like @adr_post_code + '%')
            AND ( @org_ogt_code IS NULL OR org_ogt_code = @org_ogt_code )
			AND ( @elementary_sch_flag = 0
				  OR ( (a01_enroll_kdg + a01_enroll_grade_1 +
						a01_enroll_grade_2 + a01_enroll_grade_3 +
						a01_enroll_grade_4 + a01_enroll_grade_5) > 0
					 )
				)
			AND ( @middle_sch_flag = 0
				  OR ( (a01_enroll_grade_6 + a01_enroll_grade_7 + a01_enroll_grade_8) > 0
					 )
				)
			AND ( @high_sch_flag = 0
				  OR ( (a01_enroll_grade_9 + a01_enroll_grade_10 + a01_enroll_grade_11 + a01_enroll_grade_12) > 0
					 )
				)
			AND ( @early_ed_flag = 0
				  OR a01_enroll_ee_fte > 0 )

			AND ( @higher_ed_flag = 0
				  OR
					a01_enroll_higher_ed > 0
				)
			AND ( @enrollment_low_limit is null
				 OR a01_calc_enroll_total >= @enrollment_low_limit)
			AND ( @enrollment_high_limit is null
				  --OR a01_calc_enroll_k12_total <= @enrollment_high_limit
				  OR a01_calc_enroll_total <= @enrollment_high_limit
				)
			AND ( @i_20_flag = 0
				  OR a01_i20_flag = 1
				)
			AND ( @special_needs_flag = 0
				  OR a01_special_needs_prog_flag = 1
				)
			AND ( @efl_flag = 0
				  OR a01_efl_prog_flag = 1
				)
			AND ( @online_ed_flag = 0
				  OR a01_online_ed_flag = 1
				)
			AND ( @homeschool_flag = 0
				  OR a01_homeschool_prog_flag = 1
				)
			AND ( @boarding_school_flag = 0
				  OR a01_boarding_school_flag = 1
				)
-- Handle country
			AND ( @adr_country IS NULL --or adr_country = @adr_country
				  --OR @adr_country = 'United States'    shuning 4/4/2015
                  OR ( SELECT COUNT(*)
						FROM co_address (nolock)
						join co_customer_x_address (nolock) on cxa_adr_key = adr_key
						WHERE adr_cst_key_owner = cst_key
						AND adr_delete_flag = 0
						AND adr_country = @adr_country
                     ) > 0
                )
-- Handle other accreditation -- it's a relationship
			AND ( @other_accred_flag = 0
				  OR ( SELECT COUNT(*)
						FROM co_customer_x_related_association  (nolock)
						JOIN co_related_association (nolock) ON rca_rla_key=rla_key
							AND rca_delete_flag = 0
						WHERE rca_cst_key = cst_key
							AND rla_delete_flag = 0

					 ) > 0
				)
-- Handle HEAP accreditation -- it's a user defined demographic
			AND (@heap_flag = 0
					OR (odm_custom_text_02 = 'HEAP'
					and odm_custom_date_04 > GETDATE())
				)
-- Handle ACSI accreditation -- it's a user defined demographic
			AND ( @acsi_flag = 0
					OR (odm_custom_text_02 = 'Accredited'
					and odm_custom_date_04 > GETDATE())
				)
-- Handle STAR accreditation -- it's a user defined demographic
			AND ( @star_flag = 0
					OR odm_custom_text_02 = 'STAR')
	AND orgAdr.adr_country not in (select sen_country from client_acsi_sensitive_country where sen_delete_flag = 0)   --7/21/2015


	/* 7/21/2015
	 And( EXISTS (
					/*the school is a member itself*/
					SELECT mbr_key
					FROM dbo.mb_membership (NOLOCK)
					WHERE (mbr_cst_key = cst_key AND mbr_delete_flag = 0 AND mbr_terminate_date IS NULL)
					)

		OR  EXISTS (
					/*the school is an affiliate and their system office is a member*/
					SELECT mpr_key
					FROM dbo.mb_membership_proxy (NOLOCK)
					join mb_membership (nolock) on mpr_mbr_key = mbr_key
					WHERE mpr_cst_key = cst_key AND mpr_delete_flag = 0
					AND mbr_delete_flag = 0 AND mbr_terminate_date IS NULL
					)
				)

    ORDER BY 2,3,4,5   */

	FOR XML PATH('school'), ROOT('schools')


END
